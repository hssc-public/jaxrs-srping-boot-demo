package hu.dpc.edu.jaxrsdemo;

import hu.dpc.edu.jaxrsdemo.services.HelloService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class JaxrsDemoApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private URI uriOf(String path) {
        return URI.create("http://localhost:" + port + path);
    }

    @MockBean
    private HelloService helloService;

    @DisplayName("GET /rest/hello/world returns with helloService.hello()")
    @Test
    public void contextLoads() {
        String helloResponse = "xxxHelloResponseXXX";
        given(helloService.hello())
                .willReturn(helloResponse);

        //WHEN
        final String helloWorldResponse = restTemplate.getForObject(uriOf("/rest/hello/world"), String.class);
        assertThat(helloWorldResponse).isEqualTo(helloResponse);
    }

}
